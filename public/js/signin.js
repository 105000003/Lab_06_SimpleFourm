function initApp() {
    // Login with Email/Password
    var txtEmail = document.getElementById('inputEmail');
    var txtPassword = document.getElementById('inputPassword');
    var btnLogin = document.getElementById('btnLogin');
    var btnGoogle = document.getElementById('btngoogle');
    var btnSignUp = document.getElementById('btnSignUp');
    var provider = new firebase.auth.GoogleAuthProvider();

    btnLogin.addEventListener('click', function () {
        firebase.auth().signInWithEmailAndPassword(txtEmail, txtPassword).catch(function(error) { 
            // Handle Errors here.
            var errorCode = error.code;
            var errorMessage = error.message;
        });
        window.location.href="index.html";
        /// TODO 2: Add email login button event
        ///         1. Get user input email and password to login
        ///         2. Back to index.html when login success
        ///         3. Show error message by "create_alert" and clean input field
    });

    btnGoogle.addEventListener('click', function () {
        firebase.auth().signInWithPopup(provider).then(function(result) { 
            // This gives you a Google Access Token. You can use it to access the Google API. 
            var token = result.credential.accessToken;
            // The signed-in user info. var user = result.user; 
        }).catch(function(error) { 
            // Handle Errors here. var errorCode = error.code; 
            var errorMessage = error.message; 
            // The email of the user's account used.
            var email = error.email; 
            // The firebase.auth.AuthCredential type that was used. 
            var credential = error.credential; 
            create_alert(error,errormessage);
        });
        create_alert(success,Message);
        window.location.href="index.html";
        /// TODO 3: Add google login button event
        ///         1. Use popup function to login google
        ///         2. Back to index.html when login success
        ///         3. Show error message by "create_alert"
    });

    btnSignUp.addEventListener('click', function () {
        firebase.auth().createUserWithEmailAndPassword(txtEmail, txtPassword).catch(function(error) { 
            // Handle Errors here. 
            var errorCode = error.code; 
            var errorMessage = error.message; 
        });
        create_alert(success,Message);
        txtPassword.value = '';
        txtEmail.value = '';
        /// TODO 4: Add signup button event
        ///         1. Get user input email and password to signup
        ///         2. Show success message by "create_alert" and clean input field
        ///         3. Show error message by "create_alert" and clean input field
    });
}

// Custom alert
function create_alert(type, message) {
    var alertarea = document.getElementById('custom-alert');
    if (type == "success") {
        str_html = "<div class='alert alert-success alert-dismissible fade show' role='alert'><strong>Success! </strong>" + message + "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
        alertarea.innerHTML = str_html;
    } else if (type == "error") {
        str_html = "<div class='alert alert-danger alert-dismissible fade show' role='alert'><strong>Error! </strong>" + message + "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
        alertarea.innerHTML = str_html;
    }
}

window.onload = function () {
    initApp();
};